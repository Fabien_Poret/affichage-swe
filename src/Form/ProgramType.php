<?php

namespace App\Form;

use App\Entity\Program;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shortTitle', TextType::class, [
                'label_format' => 'Titre de l\'évenement : ',
                'attr' => array('class' => 'formShortTitle'),
                'label_attr'=> array('class'=> 'formShortTitle')
            ])
            ->add('content', TextareaType::class, [
                'label_format' => 'Description du programme : ',
                'attr' => array('class' => 'ckeditor formContent'),
                'label_attr'=> array('class'=> 'formShortTitle')
            ])
            ->add('date', DateTimeType::class, [
                'label_format' => 'Date de publication : ',
                'attr' => array('class' => 'formDatetime'),
                'label_attr'=> array('class'=> 'formDatetime')
            ])
            ->add('alerte', ChoiceType::class, [
                'label_format' => 'Niveau d\'alerte : ',
                'attr' => array('class' => 'formShortTitle'),
                'label_attr'=> array('class'=> 'formShortTitle'),
                'choices' => [
                    'Programme' => 'Programme', 
                    'Important' => 'Important', 
                    'Urgent' => 'Urgent',
                ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Program::class,
        ]);
    }
}
