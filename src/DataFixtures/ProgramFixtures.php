<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Program;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProgramFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setEmail('admin@swe.fr')
            //adminSWE2019
            ->setPassword('$argon2i$v=19$m=65536,t=4,p=1$TlhYaGRQVVFpeFZINDhKeQ$I0B+Z62tDA4QwuzFSOJZzSt50PcncdobS+6yTnsPcb0');
        $manager->persist($user);

        $program = new Program();
        $datetime = new DateTime('2019-10-06 08:00:00');
        $program->setShortTitle('Test à supp')
            ->setContent('à supp')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);


        $program = new Program();
        $datetime = new DateTime('2019-10-11 18:30:00');
        $program->setShortTitle('Insert coins')
            ->setContent('Accueil des participants')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-11 18:45:00');
        $program->setShortTitle('Select your character')
            ->setContent('Ice-breaker (faire connaissance)')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);
        
        $program = new Program();
        $datetime = new DateTime('2019-10-11 19:00:00');
        $program->setShortTitle('Cinématique')
            ->setContent('Présentation du déroulé du weekend et des intervenants')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);
        
        $program = new Program();
        $datetime = new DateTime('2019-10-11 19:30:00');
        $program->setShortTitle('Press start')
            ->setContent('Pitchs des participants')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-11 20:15:00');
        $program->setShortTitle('Check point')
            ->setContent('Vote du public et dîner')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-11 20:30:00');
        $program->setShortTitle('Boss level')
            ->setContent('Annonce des idées retenues pour le weekend')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-11 22:00:00');
        $program->setShortTitle('Grow up #1')
            ->setContent('Conférence : "Le Travail en Groupe')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 08:00:00');
        $program->setShortTitle('Petit déjeuner')
            ->setContent('Démarrer la journée par un café ...')
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 10:30:00');
        $program->setShortTitle('Grow up #2')
            ->setContent("Conférence : 'L\'approche client' - 20 min")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 11:00:00');
        $program->setShortTitle('Upgrade')
            ->setContent("Rencontre avec les mentors")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 12:00:00');
        $program->setShortTitle('Check point')
            ->setContent("Déjeuner avec les participants, mentors et organisateurs")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 13:30:00');
        $program->setShortTitle('Grow up #3')
            ->setContent("Conférence 'Comment définir son business model' - 20 minutes")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 19:30:00');
        $program->setShortTitle('Apéro game')
            ->setContent("Moment de détente et de jeu avec tous les participants")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 20:00:00');
        $program->setShortTitle('Check point')
            ->setContent("Repas du soir")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-12 22:00:00');
        $program->setShortTitle('Grow up #4')
            ->setContent("Conférence 'l\'art du pitch' - 20 min")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-13 08:00:00');
        $program->setShortTitle('Petit déjeuner')
            ->setContent("Démarrer la journée par un café")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-13 10:00:00');
        $program->setShortTitle('Pitch blanc')
            ->setContent("Test du pitch dans les conditions réelles du jury")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-13 18:00:00');
        $program->setShortTitle('Final round')
            ->setContent("Présente ton projet devant le jury, les participants et les invités VIP")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        
        $program = new Program();
        $datetime = new DateTime('2019-10-13 20:00:00');
        $program->setShortTitle('Check point')
            ->setContent("Remise des prix")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $program = new Program();
        $datetime = new DateTime('2019-10-13 20:30:00');
        $program->setShortTitle('Game over')
            ->setContent("Cocktail pour clôturer le weekend")
            ->setDate($datetime)
            ->setAlerte('Programme');
        $manager->persist($program);

        $manager->flush();
    }
}
