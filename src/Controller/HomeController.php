<?php

namespace App\Controller;

use App\Repository\ProgramRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function index(ProgramRepository $repo)
    {
        date_default_timezone_set('Europe/Paris');
        $allProgram = $repo->findAll();
        $dateTime = new DateTime();
        unset($interval);
        $interval = [];
        dump($allProgram);
        foreach ($allProgram as $program) {
            if ($program->getDate() > $dateTime) {
                $interval[] = +$program->getDate()->getTimestamp();
            }
        }

        if (!empty($interval)) {
            $date = new DateTime();
            $tmp = min($interval);
            $date->setTimestamp($tmp);
            $lastNew = $repo->findOneBy(['date' => $date]);
            dump($lastNew);
        } else{
            $lastNew= [
                "shortTitle" => "Startup Weekend de Saint-lô",
                "content" => "Nous revenons prochainement, ajoute une programmation dans l'administration",
                "alerte" => "Programme",
                "date" =>new DateTime()
            ];
        }

        $recentDate = new DateTime();
        return $this->render('home/index.html.twig', [
            'date' => $recentDate,
            'lastNew' => $lastNew,
            'controller_name' => 'HomeController',
        ]);
    }
}
