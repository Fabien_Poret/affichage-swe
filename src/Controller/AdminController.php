<?php

namespace App\Controller;

use App\Entity\Program;
use App\Form\ProgramType;
use App\Repository\ProgramRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ObjectManager $manager, Request $request, Program $program = null)
    {
        if(!$program){
            $program = new Program(); 
        }

        $form = $this->createForm(ProgramType::class, $program);
        $editMode = 0;
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
                $manager->persist($program);
                $manager->flush();
                return $this->redirectToRoute('program');
            }
        }
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'formProgram' => $form->createView(),
            'editMode' => $editMode,
        ]);
    }
    
    /**
     * @Route("/admin/program", name="program")
     * @Route("/admin/program/{id}/edit", name="program_edit")
    */
    public function program(ProgramRepository $repo, ObjectManager $manager, Request $request, Program $program = null)
    {
        
        if(!$program){
            $program = new Program(); 
        }

        $programs = $repo->findAll();
        // $programs = array_reverse($allPrograms);

        if($request->isMethod('POST')) {
            if($request->request->has('program_id')) {
                $idProgram = $request->request->get('program_id');
                $actionProgram  = ($request->request->has("deleteProgram")) ? 'deleteProgram' : 'editProgram';
                if($actionProgram  === 'deleteProgram') {
                    $program = $repo->findById($idProgram);
                    $manager->remove($program[0]);
                    $manager->flush();
                    return $this->redirectToRoute('program');
                }
            }
        }


        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
                $manager->persist($program);
                $manager->flush();
                return $this->redirectToRoute('program');
            }
        }
        return $this->render('admin/program.html.twig', [
            'controller_name' => 'AdminController',
            'formProgram' => $form->createView(),
            'editMode' => $program->getId() !== null,
            'programs' => $programs,
        ]);
    }
}
